namespace :db do
  desc 'Checks whether the database exists or not'
  task :exists do
    Rake::Task['environment'].invoke
    ActiveRecord::Base.connection
    puts 'The database exists'
  rescue StandardError
    puts 'The database does not exist'
    exit 1
  else
    exit 0
  end
end
