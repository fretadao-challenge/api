# API

Ruby on Rails API of Fretadão's challenge.

Tools:
* **Ruby 2.6.4p104**
* **Rails 6.0.0**

## Setting up the environment
Docker/Docker Compose sets the environment configuration. So, the prerequisites are:

-  [Docker](https://docs.docker.com/install/)
-  [Docker Compose](https://docs.docker.com/compose/install/)

Run it with:  

```bash

docker-compose up

```

This command downloads the Ruby image and Postgres image (for the database) first if you haven't them already.

After the download, you can navigate to `http://localhost:3000.`

## About the Architecture
The first architectural decision I've made was to develop the back-end detached from the front-end. This choice is because you have more flexibility on the tools that you can use (like developing the front-end with a Javascript framework). Having these two pieces apart is good too if you want different teams/people working on different repositories (like having designers, or just front-end developers).

So, the back-end module is a JSON REST API.

### Models
Since this is a simple project, I just created one model: `Profile.` It holds all the information about a twitter profile that will be indexed.

#### Attributes
Its attributes are:

*  **name:** Holds the name provided by the user.
*  **twitter_address:** Holds the twitter URL for the profile.
*  **shortened_url:** Holds the twitter shortened URL. This URLs will redirect to the original twitter address.
*  **twitter_username:** Holds the twitter username, fetched with the WebScrapper.
*  **twitter_description:** Holds the twitter description, fetched with the WebScrapper.

#### Validations
The ensure that the data is correct, I've added some validations on the model. Here they are:

*  **Presence:** The `name` and `twitter_address` must exist.
*  **Uniqueness:** The `twitter_address` is unique, i.e., two profiles *can't* have the same twitter URL.
*  **Format:** The `twitter_address` must be a valid twitter URL, so it's checked with a REGEX.

### Endpoints

There's just one controller too, the `ProfilesController.`

The endpoints are:

*  **CRUD:** The profile controller has a complete CRUD (create, read, update and delete), plus the index endpoint (which returns all objects). On `create` and `update` the API fetches twitter information with a web scraper.

*  **Search:** The search endpoint searches for Profile's `name`, `twitter_username` or `twitter_description` (more information about this bellow). It's not case sensitive.

### Design Patterns and Good Practices

#### Search
The method `self.search_query` at `profile.rb`  programmatically generates the search query. This approach makes this query easily extensible because to add new search fields is only necessary to add a new field in the `search_fields` array. So, the `search` method acts like a `facade` to the search of all these fields.

#### Model
I kept all the business rules inside the `Profile` model (like what is the search query, what fields use on the search). It's important to keep controllers clean because they are not responsible for holding this logic. 

#### Testing
All features are tested. There are model and controller tests, both have edge-case tests too (like wrong data, etc.).
I've set up `simplecov` to show test coverage. To see it, just run:

```shell
$ rails test
```
#### Style Guide
I added `rubocop` as static code analyzer and code formatter. It's good to keep the code clean, concise, and following a standard.

#### Continuous Integration
I've used `Gitlab CI` to make continuous integration. Each commit triggers the CI that runs the linter and tests. A continuous integration could easily be set here too.

#### Version Control
All the work was made using GIT as the version control manager.

This command generates a file at `coverage/index.html.` You can open it with your browser. The test coverage is 100%.
  
### Observations

*  **CORS:** I've enables CORS at `config/initializers/cors.rb`. I know this is not production safe, but I've let it this way only for development/test purposes (so the back-end can receive requests from the front-end).

* **Short URLs:** I've used the gem `shortened` to short URLs.