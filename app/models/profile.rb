class Profile < ApplicationRecord
  include Rails.application.routes.url_helpers
  include Shortener::ShortenerHelper

  TWITTER_REGEX = /(?:http:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)/i

  before_save :create_short_url, :add_twitter_info
  before_validation :downcase_twitter_address, :add_http_prefix

  validates :name, :twitter_address, presence: true
  validates :twitter_address, format: { with: Regexp.new(TWITTER_REGEX),
                                        message: 'should be a valid twitter address' }
  validates :twitter_address, uniqueness: { case_sensitive: false }

  def self.fetch_twitter_info(url)
    require 'open-uri'
    require 'nokogiri'

    html = Nokogiri::HTML(open(url))
    info = {}
    info[:twitter_username] = get_twitter_username(html)
    info[:twitter_description] = get_twitter_description(html)
    info
  rescue Errno::ENOENT
    {}
  end

  def self.get_twitter_username(html)
    twitter_username = html.css('h2.ProfileHeaderCard-screenname.u-inlineBlock.u-dir span b')

    if !twitter_username.blank?
      twitter_username.text
    else
      ''
    end
  end

  def self.get_twitter_description(html)
    twitter_description = html.css('div.ProfileHeaderCard p')

    if !twitter_description.blank?
      twitter_description.text
    else
      ''
    end
  end

  def self.search(query)
    where(search_query, query: "#{query}").order(:name)
  end

  private

    def self.search_query
      search_fields = %w[name twitter_username twitter_description]
      search_fields.map { |s| "#{s} ~* :query" }.join(' OR ')
    end

    def downcase_twitter_address
      unless self.twitter_address.blank?
        self.twitter_address.downcase!
      end
    end

    def add_twitter_info
      unless self.twitter_address.blank?
        twitter_info = Profile.fetch_twitter_info(self.twitter_address)
        self.twitter_username = twitter_info[:twitter_username]
        self.twitter_description = twitter_info[:twitter_description]
      end
    end

    def add_http_prefix
      unless self.twitter_address.blank?
        unless self.twitter_address[/\Ahttp:\/\//] || self.twitter_address[/\Ahttps:\/\//]
          self.twitter_address = "http://#{self.twitter_address}"
        end
      end
    end

    def create_short_url
      unless self.twitter_address.blank?
        shortened_url = short_url(self.twitter_address, url_options: { host: 'localhost:3000' })
        self.shortened_url = shortened_url
      end
    end
end
