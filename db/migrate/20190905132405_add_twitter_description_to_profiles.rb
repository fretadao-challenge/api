class AddTwitterDescriptionToProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :twitter_description, :string
  end
end
