class AddTwitterUsernameToProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :twitter_username, :string
  end
end
