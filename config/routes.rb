Rails.application.routes.draw do
  resources :profiles
  get '/search', to: 'profiles#search', as: 'search'
  get '/:id' => 'shortener/shortened_urls#show'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
