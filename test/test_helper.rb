require 'simplecov'
SimpleCov.start 'rails' do
  add_filter 'jobs/'
  add_filter 'channels/'
  add_filter 'mailers/'
  add_filter 'app/controllers/application_controller.rb'
end
SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter

ENV['RAILS_ENV'] ||= 'test'

require_relative '../config/environment'
require 'rails/test_help'
require 'mocha/minitest'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  # parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def stub_twitter_info(username = 'best_username', description = "I'm the very best")
    twitter_info = {
      twitter_username: username,
      twitter_description: description
    }
    Profile.stubs(:fetch_twitter_info).returns(twitter_info)
  end
end
