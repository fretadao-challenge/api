require 'test_helper'

class ProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @profile = profiles(:alexandre)
  end

  test 'should get index' do
    get profiles_url, as: :json
    assert_response :success
  end

  test 'should create profile' do
    stub_twitter_info()

    assert_difference('Profile.count') do
      post profiles_url, params: {
        profile: {
          name: 'Best User',
          twitter_address: 'https://twitter.com/best_username'
        }
      }, as: :json
    end

    assert_response 201

    profile = JSON.parse(response.body)
    assert_equal 'Best User', profile['name']
    assert_equal 'https://twitter.com/best_username', profile['twitter_address']
    assert_equal 'best_username', profile['twitter_username']
    assert_equal "I'm the very best", profile['twitter_description']
  end

  test 'should not create invalid profile' do
    Profile.stubs(:fetch_twitter_info).returns({})

    assert_no_difference('Profile.count') do
      post profiles_url, params: {
        profile: {
          name: nil,
          twitter_address: nil
        }
      }, as: :json
    end

    assert_response :unprocessable_entity
  end

  test 'should show profile' do
    get profile_url(@profile), as: :json
    assert_response :success
  end

  test 'should update profile' do
    stub_twitter_info()

    patch profile_url(@profile), params: {
      profile: {
        name: 'New name',
        twitter_address: 'http://twitter.com/new_user'
      }
    }, as: :json
    @profile.reload

    assert_response 200
    assert_equal 'New name', @profile.name
    assert_equal 'http://twitter.com/new_user', @profile.twitter_address
    assert_equal 'best_username', @profile.twitter_username
    assert_equal "I'm the very best", @profile.twitter_description
  end

  test 'should not update invalid profile' do
    Profile.stubs(:fetch_twitter_info).returns({})

    patch profile_url(@profile), params: {
      profile: {
        name: nil,
        twitter_address: nil
      }
    }, as: :json

    assert_response :unprocessable_entity
  end

  test 'should destroy profile' do
    assert_difference('Profile.count', -1) do
      delete profile_url(@profile), as: :json
    end

    assert_response 204
  end

  test 'should search' do
    get search_url, params: {
      search_term: @profile.name
    }

    assert_response 200

    search_result = JSON.parse(response.body)
    assert_equal 1, search_result.size
    assert_equal @profile.name, search_result.first['name']
  end
end
