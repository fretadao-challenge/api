require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  def setup
    @profile = Profile.new(
      name: 'Root',
      twitter_address: 'http://twitter.com/root'
    )
  end

  test 'should save valid profile' do
    stub_twitter_info()

    assert @profile.save
    assert_equal 'Root', @profile.name
    assert_equal 'http://twitter.com/root', @profile.twitter_address
  end

  test 'should not save profile without name' do
    stub_twitter_info()

    @profile.name = nil
    refute @profile.save
  end

  test 'should not save profile without twitter address' do
    stub_twitter_info()

    @profile.twitter_address = nil
    refute @profile.save
  end

  test 'should not save profile with invalid twitter url' do
    stub_twitter_info()

    @profile.twitter_address = 'http://twiter.com/root'
    refute @profile.save
    assert_includes @profile.errors[:twitter_address], 'should be a valid twitter address'
  end

  test 'should not save profile with duplicated twitter url' do
    stub_twitter_info()

    @profile.save
    another_profile = Profile.new(name: 'Another', twitter_address: @profile.twitter_address.upcase)
    refute another_profile.save
    assert_includes another_profile.errors[:twitter_address], 'has already been taken'
  end

  test 'should get twitter info' do
    require 'open-uri'

    Profile.stubs(:get_twitter_username).returns('best_username_ever')
    Profile.stubs(:get_twitter_description).returns('Nice description')
    OpenURI.expects(:open_uri).returns('<div>some html</div>')

    info = Profile.fetch_twitter_info('https://coolurl.com')
    assert_equal 'best_username_ever', info[:twitter_username]
    assert_equal 'Nice description', info[:twitter_description]
  end

  test "should not save twitter info if can't access page" do
    require 'open-uri'

    OpenURI.expects(:open_uri).raises(Errno::ENOENT, 'error')

    info = Profile.fetch_twitter_info('https://coolurl.com')
    assert_nil info[:twitter_username]
    assert_nil info[:twitter_description]
  end

  test 'should not get twitter info if it is not found' do
    require 'open-uri'

    OpenURI.expects(:open_uri).returns('<div>some wrong html</div>')

    info = Profile.fetch_twitter_info('https://coolurl.com')
    assert_equal '', info[:twitter_username]
    assert_equal '', info[:twitter_description]
  end

  test 'should get twitter description' do
    require 'nokogiri'
    html_str = <<~HEREDOC
      <div class='ProfileHeaderCard'>
        <p>Some Description</p>
      </div>
    HEREDOC
    html = Nokogiri::HTML(html_str)
    assert_equal 'Some Description', Profile.get_twitter_description(html)
  end

  test 'should get twitter username' do
    require 'nokogiri'
    html_str = <<~HEREDOC
      <h2 class='ProfileHeaderCard-screenname u-inlineBlock u-dir'>
        <span>
          <b>my_username</b>
        </span>
      </h2>
    HEREDOC
    html = Nokogiri::HTML(html_str)
    assert_equal 'my_username', Profile.get_twitter_username(html)
  end

  test 'should add http on twitter address if it not contains it' do
    stub_twitter_info()
    profile = Profile.create(name: 'New Profile', twitter_address: 'twitter.com/new')
    assert_equal 'http://twitter.com/new', profile.twitter_address
  end

  test 'should generate shortened url' do
    stub_twitter_info()

    profile = Profile.create(name: 'New Profile', twitter_address: 'twitter.com/new')
    refute profile.shortened_url.blank?
  end

  test 'should return all objects on empty search' do
    assert_equal Profile.count, Profile.search('').count
  end

  test 'should search for profile name' do
    stub_twitter_info()
    @profile.save
    assert_includes Profile.search(@profile.name), @profile
  end

  test 'should search for twitter username' do
    stub_twitter_info()
    @profile.twitter_username = 'root_user'
    @profile.save!
    assert_includes Profile.search(@profile.twitter_username), @profile
  end

  test 'should search for twitter description' do
    stub_twitter_info(nil, "I'm a root user, very powerful")
    @profile.save!
    assert_includes Profile.search('powerful'), @profile
  end

  test 'should not find unexisting profiles' do
    assert_equal [], Profile.search('this-profile-does-not-exist')
  end
end
