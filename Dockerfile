FROM ruby:2.6.4-slim

RUN apt-get update -qq && \
    apt-get install -y nodejs \
    build-essential \
    libpq-dev

COPY . /api

WORKDIR /api

ENTRYPOINT ["sh","scripts/start.sh"]
